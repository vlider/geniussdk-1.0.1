//
//  URLEncoding.h
//  NetworkDev
//
//  Created by Rob Kelly on 11-06-23.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import <Foundation/Foundation.h>
@interface NSString (IGSDK_URLEncoding)
-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding;
@end