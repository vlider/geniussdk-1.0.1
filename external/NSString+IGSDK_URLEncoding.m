//
//  URLEncoding.m
//  NetworkDev
//
//  Created by Rob Kelly on 11-06-23.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import "NSString+IGSDK_URLEncoding.h"
@implementation NSString (IGSDK_URLEncoding)
-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding {
	return ( NSString *)CFURLCreateStringByAddingPercentEscapes(NULL,
                                                               ( CFStringRef)self,
                                                               NULL,
                                                               (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
                                                               CFStringConvertNSStringEncodingToEncoding(encoding));
}
@end