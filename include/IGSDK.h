//
//  ivokesdk_lib.h
//  ivokesdk-lib
//
//  Created by Brendan Clement on 12-05-06.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGSDK : NSObject

+(void)startInViewController:(UIViewController *)aController withImage:(UIImage *)image testMode:(BOOL)testMode;

@end
